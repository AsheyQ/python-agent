class ReportCounter:

    def __init__(self, validDumps=0, errorDumps=0, serviceCount=0, errors={}):
        self.serviceCount = serviceCount
        self.validDumps = validDumps
        self.errorDumps = errorDumps
        self.errors = errors
