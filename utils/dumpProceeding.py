import os
import shutil
import tempfile

from loguru import logger
from models.fabric import FabricMapper
from api.disk.uploading import YaDiskStorage
from utils.configReader import ConfigReader
from utils.logging import Logger
from utils.report import ReportCounter
from api.telegram.informing import TelegramInformer


class DumpProceeder:

    def __init__(self, config=ConfigReader(), path=tempfile.TemporaryDirectory(), reporter=ReportCounter()):
        self.config = config
        self.path = path
        self.reporter = reporter

    def get_backup(self, pargs):
        if not pargs.dryrun:
            Logger().setup()

        if os.path.exists('dumps_temp'):
            shutil.rmtree('dumps_temp', ignore_errors=True)
        os.makedirs('dumps_temp')

        for service in self.config.services:
            logger.debug(f"Starting dump of service: {service}")
            servicePath = os.path.join(self.path.name, service)
            os.makedirs(os.path.join(servicePath, 'Postgres'))
            os.makedirs(os.path.join(servicePath, 'Files'))
            self.reporter.serviceCount += 1
            for item in self.config.services[service]:
                curItem = self.config.services[service][item]

                dumper = FabricMapper.get_dumper(curItem['type'])
                object = FabricMapper.get_dump_object(curItem['type'])

                itemModel = object.create_backup(curItem, servicePath, item)
                result = dumper.dump(itemModel)
                output = result.stdout + result.stderr
                if result.returncode != 0:
                    logger.error(f"Failed to make dump of {itemModel.itemName} \n Error message: {output}")
                    self.reporter.errorDumps += 1
                    self.reporter.errors[itemModel.itemName] = output
                else:
                    logger.debug(f"Successful dump of {itemModel.itemName}")
                    self.reporter.validDumps += 1

            shutil.make_archive(service, 'zip', servicePath)
            shutil.move(service + '.zip', os.path.abspath('dumps_temp'))
            if not pargs.dryrun:
                YaDiskStorage().upload(os.path.join(os.path.abspath('dumps_temp'), service + '.zip'), service)

        if not pargs.dryrun:
            TelegramInformer().send_report(self.reporter)
