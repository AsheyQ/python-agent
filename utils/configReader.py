import yaml


class ConfigReader:
    def __init__(self):
        with open('./config/config.yml', 'r') as file:
            self.data = yaml.safe_load(file)
        self.env = self.data['environment']
        self.services = self.data['services']
