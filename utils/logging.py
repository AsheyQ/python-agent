import os
from utils.configReader import ConfigReader
from loguru import logger


class Logger:
    def __init__(self, config=ConfigReader(), path='_logs/'):
        self.config = config
        self.path = path

    def setup(self):
        size = self.get_logs_size()
        if size > self.config.env["logMemoryLimit"]:
            while size > self.config.env["logMemoryLimit"] * 0.5:
                for path, dirs, files in os.walk(self.path):
                    for f in files:
                        fp = os.path.join(path, f)
                        size -= os.path.getsize(fp)
                        os.remove(fp)

        logger.add(
            "_logs/debug_{time:YY-mm-dd-HH-mm-ss!UTC}.log",
            format="{time},{level},{message}",
            level="DEBUG",
            rotation="10 KB",
            colorize=True
        )

    def get_logs_size(self):
        size = 0
        for path, dirs, files in os.walk(self.path):
            for f in files:
                fp = os.path.join(path, f)
                size += os.path.getsize(fp)
        return size
