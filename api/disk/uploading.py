import datetime
import os
import yadisk
from loguru import logger
from utils.configReader import ConfigReader


class YaDiskStorage:

    def __init__(self, config=ConfigReader()):
        self.config = config

    def upload(self, file_upload, item):
        y = yadisk.YaDisk(token=self.config.env['diskToken'])
        logger.debug(f"Starting upload of service: {item}")
        if not y.exists('dumps'):
            y.mkdir('dumps')
        path = os.path.join('dumps', f'{item}_{datetime.datetime.today().strftime("%d-%m-%y_%H-%M-%S")}.zip')
        y.upload(file_upload, path)
        os.remove(file_upload)
        logger.debug(f"Successful upload of service: {item}")

