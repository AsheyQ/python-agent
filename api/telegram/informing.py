from datetime import date

from utils.configReader import ConfigReader
import requests


class TelegramInformer:

    def __init__(self, config=ConfigReader()):
        self.config = config

    def send_report(self, reporter):
        message = f'Дамп {date.today()} завершён успешно\n' \
                  f'Сервисов сдамплено: {reporter.serviceCount}\n' \
                  f'Всего элементов: {reporter.validDumps + reporter.errorDumps}\n' \
                  f'Успешных: {reporter.validDumps}\n' \
                  f'Ошибок: {reporter.errorDumps}\n\n'

        if reporter.errorDumps != 0:
            for error in reporter.errors:
                message += f'Ошибка при дампе элемента {error}: {reporter.errors[error]}'

        params = {
            'chat_id': self.config.env['tgId'],
            'text': message,
        }

        requests.get('https://api.telegram.org/bot' + self.config.env['botToken'] + '/sendMessage', params=params)

