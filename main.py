
from utils.dumpProceeding import DumpProceeder
import argparse


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("-d", "--dryrun", default=False)

    return parser.parse_args()


def run_dump(args=None):
    args = parse_args()
    DumpProceeder().get_backup(args)


if __name__ == '__main__':
    run_dump()

