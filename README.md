# python-agent


Python agent for performing database and volumes dumps from docker containers

- Container and property addresses are set in config.yml

- Then, based on the config parsing, the agent performs a dump and archives it

- Uses integration with cloud storage to upload data and integration with Telegram API for user notifications

- --dryrun parameter for local use(without uploading to cloud & notifications)

- Logging by Loguru
