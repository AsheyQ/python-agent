
from abc import ABC, abstractmethod


class Backup(ABC):
    def __init__(self, containerName, itemName, servicePath):
        self.containerName = containerName
        self.itemName = itemName
        self.servicePath = servicePath

    @abstractmethod
    def print(self):
        pass

    @abstractmethod
    def to_dict(self):
        pass


class PGBackup(Backup):

    def __init__(self, dbName, dbUser, containerName, itemName, servicePath):
        super().__init__(containerName, itemName, servicePath)
        self.dbName = dbName
        self.dbUser = dbUser

    def print(self):
        print(self.dbName, self.dbUser, self.containerName)

    def to_dict(self):
        return {
            'containerName': self.containerName,
            'dbName': self.dbName,
            'dbUser': self.dbUser,
        }


class FileBackup(Backup):

    def __init__(self, path, containerName, itemName, servicePath):
        super().__init__(containerName, itemName, servicePath)
        self.path = path

    def print(self):
        print(self.path, self.containerName)

    def to_dict(self):
        return {
            'containerName': self.containerName,
            'path': self.path,
        }
