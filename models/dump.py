import subprocess
from loguru import logger


class PGDumper:
    @staticmethod
    def dump(item):

        cmd = f'docker exec -t {item.containerName}' \
              f' pg_dump -c -U {item.dbUser} {item.dbName} -O ' \
              f'> {item.servicePath}/Postgres/{item.itemName}.sql'
        logger.debug(f"Starting dump of item: {item.itemName}\n{item.to_dict()}\nCMD: {cmd}")

        return subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)


class FileDumper:
    @staticmethod
    def dump(item):
        cmd = f'docker cp {item.containerName}:{item.path} {item.servicePath}/Files/{item.itemName}'
        logger.debug(f"Starting dump of item: {item.itemName}\n{item.to_dict()}\nCMD: {cmd}")

        return subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
