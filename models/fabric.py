import enum

from models.backup import PGBackup, FileBackup
from models.dump import PGDumper, FileDumper


class PGBackupFabric:

    @staticmethod
    def create_backup(item, servicePath, itemName):
        return PGBackup(
            containerName=item['container_name'],
            dbName=item['db_name'],
            dbUser=item['db_user'],
            itemName=itemName,
            servicePath=servicePath
        )


class FileBackupFabric:
    @staticmethod
    def create_backup(item, servicePath, itemName):
        return FileBackup(
            containerName=item['container_name'],
            path=item['path'],
            itemName=itemName,
            servicePath=servicePath
        )


class FabricMapper(enum.Enum):
    PG = {
        'dumper': PGDumper(),
        'object': PGBackupFabric()
    }

    FILES = {
        'dumper': FileDumper(),
        'object': FileBackupFabric()
    }

    @staticmethod
    def get_dumper(type):
        return FabricMapper[type].value['dumper']

    @staticmethod
    def get_dump_object(type):
        return FabricMapper[type].value['object']
